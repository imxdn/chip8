#ifndef CPU_HPP
#define CPU_HPP

#include <cstdint>

// 4KB RAM memory
uint8_t mem[4096];
// 8-bit general purpose registers Vx
uint8_t V[16];
// 16-bit I register
uint16_t I;
// 8-bit delay timer
uint8_t dt;
// 8-bit sound timer
uint8_t st;
// 16-bit program counter
uint16_t pc;
// 8-bit stack pointer
uint8_t sp;
// 16-bit stack of size 16
uint16_t stack[16];
// CPU functions
void cpu_init();
void emulate_cycle();
void opcode_fault(uint16_t);

#endif
