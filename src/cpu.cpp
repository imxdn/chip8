#include <iostream>
#include <cstdlib>
#include "cpu.hpp"
#include "display.hpp"
#include "keyboard.hpp"

static uint8_t fontset[80] = {
    0xF0 ,0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80  //F
};

void cpu_init() {
    // Initilize CPU and memory variables
    memset(&mem, 0, 4096);
    memset(&V, 0, 16);
    memset(&stack, 0, 16 * 2);
    memset(&disp, 0, 64 * 32);
    sp = -1;

    // Load fontset into memory starting
    for (size_t i = 0; i < 80; ++i) {
        mem[i] = fontset[i];
    }
}

void opcode_fault(uint16_t opcode) {
    std::cerr << "Illegal or unrecognized opcode encountered, exiting..." << std::endl;
    exit(EXIT_FAILURE);
}

void emulate_cycle() {

    // Fetch opcode from memory
    uint16_t opcode = (mem[pc] << 0x8 | mem[pc + 1]);

    // Decode and execute opcode
    switch (opcode & 0xF000) {

        case 0x0000: {
            switch (opcode & 0x00FF) {
                // 00E0: Clear the screen
                case 0x00E0: {
                    memset(disp, 0, 64 * 32);
                    pc += 2;
                    break;
                }
                // 00EE: Return from subroutine
                case 0x00EE: {
                    pc = stack[sp--];
                    pc += 2;
                    break;
                }

                default: {
                    opcode_fault(opcode);
                }
            }
            break;
        }

        // 1NNN: Jump to address NNN
        case 0x1000: {
            pc = opcode & 0x0FFF;
            break;
        }

        // 2NNN: Call subroutine at NNN
        case 0x2000: {
            stack[++sp] = pc;
            pc = opcode & 0x0FFF;
            break;
        }

        // 3XNN: If VX == NN then skip the next instruction
        case 0x3000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t NN = (opcode & 0x00FF);

            if (V[X] == NN) pc += 2;
            pc += 2;
            break;
        }

        // 4XNN: If VX != NN then skip the next instruction
        case 0x4000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t NN = (opcode & 0x00FF);

            if (V[X] != NN) pc += 2;
            pc += 2;
            break;
        }

        // 5XY0: If VX == VY then skip the next instruction
        case 0x5000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t Y = (opcode & 0x00F0) >> 0x4;

            if (V[X] == V[Y]) pc += 2;
            pc += 2;
            break;
        }

        // 6XNN: Set VX to NN
        case 0x6000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t NN = (opcode & 0x00FF);

            V[X] = NN;
            pc += 2;
            break;
        }

        // 7XNN: Increment VX by NN
        case 0x7000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t NN = (opcode & 0x00FF);

            V[X] += NN;
            pc += 2;
            break;
        }

        // 8XY[0-7/E]
        case 0x8000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t Y = (opcode & 0x00F0) >> 0x4;

            switch (opcode & 0x000F) {
                // 8XY0: Sets VX to the value of VY
                case 0x0000: {
                    V[X] = V[Y];
                    pc += 2;
                    break;
                }

                // 8XY1: Sets VX to VX OR VY
                case 0x0001: {
                    V[X] |= V[Y];
                    pc += 2;
                    break;
                }

                // 8XY2: Sets VX to VX AND VY
                case 0x0002: {
                    V[X] &= V[Y];
                    pc += 2;
                    break;
                }

                // 8XY3: Sets VX to VX XOR VY
                case 0x0003: {
                    V[X] ^= V[Y];
                    pc += 2;
                    break;
                }

                // 8XY4: Adds VY to VX. If carry VF set to 1, 0 otherwise
                case 0x0004: {
                    V[X] += V[Y];
                    if (V[X] > 0xFF) V[0xF] = 0x01;
                    else V[0xF] = 0x00;
                    pc += 2;
                    break;
                }

                // 8XY5: VY subtracted from VX. If borrow, VF set to 0, 1 otherwise
                case 0x0005: {
                    if (V[X] > V[Y]) V[0xF] = 0x01;
                    else V[0xF] = 0x00;
                    pc += 2;
                    break;
                }

                // 8XY6: Shifts VX right by one
                case 0x0006: {
                    V[0xF] = V[X] & 0x01;
                    V[X] >>= 1;
                    pc += 2;
                    break;
                }

                // 8XY7: Sets VX to VY minus VX. If borrow, VF set to 0, 1 otherwise
                case 0x0007: {
                    if (V[Y] > V[X]) V[0xF] = 0x01;
                    else V[0xF] = 0x00;
                    V[X] = V[Y] - V[X];
                    pc += 2;
                    break;
                }

                // 8XYE: Shifts VX left by one
                case 0x000E: {
                    V[0xF] = V[X] & 0x80;
                    V[X] <<= 1;
                    pc += 2;
                    break;
                }

                default: {
                    opcode_fault(opcode);
                }
            }
            break;
        }

        // 9XY0: Skip the next instruction if VX doesn't equal VY
        case 0x9000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t Y = (opcode & 0x00F0) >> 0x4;

            if (V[X] != V[Y]) pc += 2;
            pc += 2;
            break;
        }

        // ANNN: Sets I to the address NNN
        case 0xA000: {
            I = opcode & 0x0FFF;
            pc += 2;
            break;
        }

        // BNNN: Jump to the address NNN plus V0
        case 0xB000: {
            pc = (opcode & 0x0FFF) + V[0x0];
            break;
        }

        // CXNN: Set VX to bitwise AND operation on a random number and NN
        case 0xC000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t NN = opcode & 0x00FF;

            V[X] = (std::rand() % 256) & NN;
            pc += 2;
            break;

        }

        // DXYN: Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels
        case 0xD000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;
            uint16_t Y = (opcode & 0x00F0) >> 0x4;
            uint16_t N = opcode & 0x000F;
            uint16_t pixel, tmp;
            V[0xF] = 0;

            for(size_t i = 0; i < N; ++i) {
                pixel = mem[I+i];
                for (size_t j = 0; j < 8; ++j) {
                    tmp = disp[(Y + i) % 32][(X + j) % 64];
                    disp[(Y + i) % 32][(X + j) % 64] ^= (pixel & (0x80 >> j)) >> (7-j);

                    if (tmp == 1 &&
                        disp[(Y + i) % 32][(X + j) % 64] == 0) V[0xF] = 1;
                }
            }
            pc += 2;
            break;
        }

        case 0xE000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;

            switch (opcode & 0x00FF) {
                // EX9E: Skips the next instruction if the key stored in VX is pressed
                case 0x009E: {
                    if (keyboard[V[X]] != 0) pc += 2;
                    pc += 2;
                    break;
                }

                // Skips the next instruction if the key stored in VX isn't pressed
                case 0x00A1: {
                    if (keyboard[V[X]] == 0) pc += 2;
                    pc += 2;
                    break;
                }

                default: {
                    opcode_fault(opcode);
                }
            }
            break;
        }

        case 0xF000: {

            uint16_t X = (opcode & 0x0F00) >> 0x8;

            switch (opcode & 0x00FF) {
                // FX07: Sets VX to the value of the delay timer
                case 0x0007: {
                    V[X] = dt;
                    pc += 2;
                    break;
                }

                // FX0A: Key press awaited, then stored in VX (Blocking)
                case 0x000A: {

                    uint16_t X = (opcode & 0x0F00) >> 0x8;

                    bool poll_flag = true;
                    while (poll_flag) {
                        SDL_WaitEvent(&event);
                        switch(event.type) {
                            case SDL_QUIT: {
                                exit(0);
                            }

                            case SDL_KEYDOWN: {
                                poll_flag = false;
                                for (size_t i = 0; i < 16; ++i) {
                                    if (keymap[i] == event.key.keysym.sym) V[X] = keymap[i];
                                }
                                break;
                            }
                        }
                    }
                    break;
                }

                // FX15: Sets the delay timer to VX
                case 0x0015: {
                    dt = V[X];
                    pc += 2;
                    break;
                }

                // FX18: Sets the sound timer to VX
                case 0x0018: {
                    st = V[X];
                    pc += 2;
                    break;
                }

                // FX1E: Add VX to I
                case 0x001E: {
                    I += V[X];
                    pc += 2;
                    break;
                }

                // FX29: Sets I to the location of the sprite for the character in VX
                case 0x0029: {

                    uint16_t X = (opcode & 0x0F00) >> 0x8;

                    // Characters in memory from 0x000 and size of 0x5 byte each
                    I = V[X] * 0x5;
                    pc += 2;
                    break;
                }

                // FX33: Stores the binary-coded decimal representation of VX,
                //  with the most significant of three digits at the address in I,
                //  the middle digit at I plus 1, and the least significant digit at I plus 2
                case 0x0033: {
                    mem[I] = V[X] / 100;
                    mem[I + 1] = (V[X] / 10) % 10;
                    mem[I + 2] = (V[X] % 100) % 10;
                    break;
                }

                // FX55: Stores V0 to VX (including VX) in memory starting at address I
                case 0x0055: {
                    for (size_t i = 0; i <= X; ++i) mem[I + i] = V[i];
                    pc += 2;
                    break;
                }

                // FX65: Fills V0 to VX (including VX) with values from memory starting at address I
                case 0x0065: {
                    for (size_t i = 0; i <= X; ++i) V[i] = mem[I + i];
                    pc += 2;
                    break;
                }

                default: {
                    opcode_fault(opcode);
                }
            }
        }

        default: {
            opcode_fault(opcode);
        }
    }

    if (dt > 0) --dt;
    if (st > 0) --st; // Handle sound TODO
}
